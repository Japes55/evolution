﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PopulationToolTipHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject MouseOverToolTipTemplate;
    GameObject _mouseOverToolTip;
    MouseOverToolTip _mouseOverToolTipScript;
    GameObject _tooltipsGroup; //need to make tooltips the child of something that will draw on top of other stuff
    Creature _myCreature;

    // Use this for initialization
    void Start () {
        _tooltipsGroup = GameObject.Find("ToolTips");
        
        _mouseOverToolTip = Instantiate(MouseOverToolTipTemplate, Vector3.zero, Quaternion.identity,
                                            _tooltipsGroup.transform) as GameObject;
        _mouseOverToolTip.transform.localScale = Vector3.one;
        _mouseOverToolTipScript = _mouseOverToolTip.GetComponent<MouseOverToolTip>();
        _myCreature = GetComponentInParent<Creature>();

        _mouseOverToolTipScript.Init();
        _mouseOverToolTip.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
        _mouseOverToolTip.SetActive(true);
        _mouseOverToolTipScript.SetText(_myCreature.PopulationReasons);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _mouseOverToolTip.SetActive(false);
    }

    void OnDestroy()
    {
        Destroy(_mouseOverToolTip);
    }

}
