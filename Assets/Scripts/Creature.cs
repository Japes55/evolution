﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Creature : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    //order is important here, i use it for sorting in the game controller
    public enum eDietType
    {
        herbivore,
        omnivore,
        carnivore
    }

    public enum ePredatorPreyRelationship
    {
        nothing,
        predator,
        prey
    }

    public enum eSize
    {
        tiny    = 1,
        small   = 2,
        medium  = 3,
        large   = 4,
        huge    = 5
    }

    //Things that can be tweaked
    public eDietType Diet
    {
        get
        {
            return _diet;
        }
        set
        {
            _diet = value;
            UpdateEvolutionCost();
        }
    }
    private eDietType _diet;
    private eDietType _dietBeforeEvolution;

    public eSize Size
    {
        get
        {
            return _size;
        }
        set
        {
            _size = value;
            UpdateEvolutionCost();
        }
    }
    private eSize _size;
    private eSize _sizeBeforeEvolution;

    public bool IsPlayer { get; private set; }
    //logic variables
    public string Name { get; private set; }

    public int Population
    {
        get
        {
            return _population;
        }
        set
        {
            if (value != _population)
            {
                //PopulationReasons += "Setting population to " + value + "\n";
                _population = value;
                //if(_population < 0)
                //{
                //    _population = 0;
                //}
                UpdateUI_Population(_population);
                BioMass = value * (int)Size;
            }
        }
    }
    int _population;

    public float MaxBioMass //maximum biomass of this creature that the environment can sustain.
    {
        get
        {
            return _maxBioMass;
        }
        set
        {
            _maxBioMass = value;
            //PopulationReasons += "Setting MaxBioMass to " + value + "\n";
            if (BioMass > value)
            {
                PopulationReasons += "Population capped to " + (int)(value/(float)Size) + " due to food limits\n";
                BioMass = value;
            }
        }
    }
    float _maxBioMass;

    public float BioMass
    { 
        get
        {
            return _bioMass;
        }
        set
        {
            if (value != _bioMass)
            {
                //PopulationReasons += "Setting BioMass to " + value + "\n";
                _bioMass = value;
                Population = (int)(value / (float)(int)Size);
            }
        }
    }
    float _bioMass;

    public float EvolutionCost
    {
        get
        {
            return _evolutionCost;
        }
        private set
        {
            _evolutionCost = value;
            if (IsPlayer)
            {
                UpdateUI_EvolutionCost(value);
            }
        }
    }
    float _evolutionCost;

    public List<Creature> Predators
    {
        get
        {
            if (_predators == null)
            {
                _predators = new List<Creature>();
            }
            return _predators;
        }
        set
        {
            _predators = value;
            UpdateUI_Predators(_predators);
        }
    }
    List<Creature> _predators;

    public List<Creature> Prey
    {
        get
        {
            if(_prey == null)
            {
                _prey = new List<Creature>();
            }
            return _prey;
        }
        set
        {
            _prey = value;
            UpdateUI_Prey(_prey);
        }
    }
    List<Creature> _prey;

    public bool IsExtinct { get; private set; }

    EvolutionGameController _evoGameController;

    public string PopulationReasons;
    bool _highlightPredatorsPrey;

    //UI element handles
    Text _nameText;
    Text _populationText;
    Dropdown _dietDropDown;
    Dropdown _sizeDropDown;
    Text _predatorsListText;
    Text _preyListText;
    GameObject _extinctIndicator;
    GameObject _isPlayerBorder;
    GameObject _predatorPreyborder;
    GameObject _nextTurnButton;
    Text _nextTurnText;
    GameObject _evolutionCostSection;
    Text _evolutionCostValue;
    GameObject _predatorPreyIndicator;
    Text _predatorPreyText;

    // Use this for initialization
    void Start () {
        _highlightPredatorsPrey = false;
    }

    public void Initialise(bool isPlayer)
    {
        _evoGameController = GameObject.Find("EvolutionGameController").GetComponent<EvolutionGameController>();

        //get references to UI elements
        _nameText = transform.Find("Panel/Heading/Name").gameObject.GetComponent<Text>();
        _populationText = transform.Find("Panel/Population/Panel/PopulationLabel").gameObject.GetComponent<Text>();

        _dietDropDown = transform.Find("Panel/Diet/DietDropDown").gameObject.GetComponent<Dropdown>();
        _dietDropDown.onValueChanged.AddListener(delegate {
            onDietChange((eDietType)_dietDropDown.value);
        });

        _sizeDropDown = transform.Find("Panel/Size/SizeDropDown").gameObject.GetComponent<Dropdown>();
        _sizeDropDown.onValueChanged.AddListener(delegate {
            onSizeChange((eSize)_sizeDropDown.value);
        });

        _predatorsListText = transform.Find("Panel/Predators/PredatorsList").gameObject.GetComponent<Text>();
        _preyListText = transform.Find("Panel/Prey/PreyList").gameObject.GetComponent<Text>();
        _extinctIndicator = transform.Find("Panel/ExtinctIndicator").gameObject;
        _extinctIndicator.SetActive(false);
        _isPlayerBorder = transform.Find("IsPlayerBorder").gameObject;
        _predatorPreyborder = transform.Find("PredatorPreyBorder").gameObject;
        _nextTurnButton = transform.Find("Panel/NextTurnButton").gameObject;
        _nextTurnText = transform.Find("Panel/NextTurnButton/Text").GetComponent<Text>();
        _evolutionCostSection = transform.Find("Panel/EvolutionCost").gameObject;
        _evolutionCostValue = transform.Find("Panel/EvolutionCost/EvolutionCostValue").GetComponent<Text>();
        _predatorPreyIndicator = transform.Find("PredatorPreyIndicator").gameObject;
        _predatorPreyText = transform.Find("PredatorPreyIndicator").GetComponent<Text>();
        
        IsPlayer = isPlayer;
        if (!IsPlayer)
        {
            _dietDropDown.interactable = false;
            _sizeDropDown.interactable = false;
        }
        else
        {
            _nameText.fontSize++;
            _isPlayerBorder.SetActive(true);
            _nextTurnButton.SetActive(true);
            _nextTurnButton.GetComponent<Button>().onClick.AddListener(_evoGameController.OnNextTurnClick);
        }

        SetInitialParameters();
    }
	
    void SetInitialParameters()
    {
        //randomise shit
        Diet = Utilities.GetRandomEnumIndex<eDietType>();
        Size = Utilities.GetRandomEnumIndex<eSize>();
        Name = IsPlayer? "You" : Utilities.GenerateRandomName();

        //update options
        List <eSize> sizeOptions;
        List<eDietType> dietOptions;
        GetAvailableOptions(out sizeOptions, out dietOptions);

        PopulationReasons = "";

        //update frontend
        UpdateUI_DietRange(dietOptions);
        UpdateUI_SizeRange(sizeOptions);
        UpdateUI_Diet(Diet);
        UpdateUI_Size(Size);
        UpdateUI_Name(Name);
    }

    // Update is called once per frame
    void Update () {
        if (_highlightPredatorsPrey)
        {
            _evoGameController.HighlightPredatorsPrey(this);
        }
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
        _highlightPredatorsPrey = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _highlightPredatorsPrey = false;
        _evoGameController.HighlightPredatorsPrey(null);
    }

    public void Highlight(ePredatorPreyRelationship rel)
    {
        switch (rel)
        {
            case ePredatorPreyRelationship.nothing:
                _predatorPreyborder.SetActive(false);
                _predatorPreyIndicator.SetActive(false);
                break;
            case ePredatorPreyRelationship.predator:
                {
                    _predatorPreyborder.SetActive(true);
                    Image border = _predatorPreyborder.GetComponent<Image>();
                    border.color = new Color(1.0f, 0.0f, 0.0f, border.color.a);

                    _predatorPreyIndicator.SetActive(true);
                    _predatorPreyText.text = "PREDATOR";
                    _predatorPreyText.color = border.color;
                }
                break;
            case ePredatorPreyRelationship.prey:
                {
                    _predatorPreyborder.SetActive(true);
                    Image border = _predatorPreyborder.GetComponent<Image>();
                    border.color = new Color(0.0f, 1.0f, 0.0f, border.color.a);

                    _predatorPreyIndicator.SetActive(true);
                    _predatorPreyText.text = "PREY";
                    _predatorPreyText.color = border.color;
                }
                break;
            default:
                break;
        }
    }

    public void UpdateEvolutionCost()
    {
        float cost = 0.0f;
        cost += Mathf.Abs((float)_diet - (float)_dietBeforeEvolution) * 0.1f;
        cost += Mathf.Abs((float)_size - (float)_sizeBeforeEvolution) * 0.1f;
        EvolutionCost = cost;
    }

    public void ResetEvolutionCost()
    {
        EvolutionCost = 0.0f;
    }

    public void SetExtinct()
    {
        IsExtinct = true;
        UpdateUI_Extinct(true);
        _predatorPreyborder.SetActive(false);
    }

    //randomly evolves this creature.
    //returns the population cost incurred by the evolution.
    public void Mutate()
    {
        //get options
        List<eSize> sizeOptions;
        List<eDietType> dietOptions;
        GetAvailableOptions(out sizeOptions, out dietOptions);

        if(Random.Range(0.0f, 1.0f) > 0.5f)
        {
            if (Random.Range(0.0f, 1.0f) > 0.5f)
            {
                eDietType newDiet = dietOptions[Random.Range(0, dietOptions.Count)];
                Diet = newDiet;
            }
            else
            {
                eSize newSize = sizeOptions[Random.Range(0, sizeOptions.Count)];
                Size = newSize;
            }
        }

        Evolve();
    }

    //apply changes and pay evolution cost
    public void Evolve()
    {
        //pay for evolution
        if (EvolutionCost > 0)
        {
            PopulationReasons += "-" + (int)Population* EvolutionCost + " due to evolution\n";
            Population = (int)((float)Population * (1 - EvolutionCost)); //round down here i guess
        }

        //set stuff
        _sizeBeforeEvolution = Size;
        _dietBeforeEvolution = Diet;
        UpdateEvolutionCost();

        //update frontend
        //get options
        List<eSize> sizeOptions;
        List<eDietType> dietOptions;
        GetAvailableOptions(out sizeOptions, out dietOptions);

        UpdateUI_DietRange(dietOptions);
        UpdateUI_SizeRange(sizeOptions);
        UpdateUI_Diet(Diet);
        UpdateUI_Size(Size);
    }

    //does the logic of checking which options are available to the creature in its current state
    void GetAvailableOptions(out List<eSize> sizes, out List<eDietType> diets)
    {
        sizes = new List<eSize>();
        diets = new List<eDietType>();

        foreach (eSize size in System.Enum.GetValues(typeof(eSize)))
        {
            //allowed to move by 2
            if(size >= (Size - 2) && size <= (Size + 2))
            {
                sizes.Add(size);
            }
        }
        foreach (eDietType diet in System.Enum.GetValues(typeof(eDietType)))
        {
            //allowed to move by 1
            //allowed to move by 2
            if (diet >= (Diet - 1) && diet <= (Diet + 1))
            {
                diets.Add(diet);
            }
        }
    }

    public void SetMyTurn(bool myTurn)
    {
        UpdateUI_MyTurn(myTurn);
    }

    //Front end callback methods------------------------------------------------------------------------
    public void onDietChange(eDietType diet)
    {
        if (_dietDropDown.options[(int)diet].text != "---")
        {
            Diet = diet;
        }
        UpdateUI_Diet(Diet);
    }

    public void onSizeChange(eSize size)
    {
        if (_sizeDropDown.options[(int)size].text != "---")
        {
            Size = size + 1;
        }
        UpdateUI_Size(Size);
    }

    //Front end update methods------------------------------------------------------------------------
    void UpdateUI_Diet(eDietType diet)
    {
        _dietDropDown.value = (int)Diet;
    }
    void UpdateUI_DietRange(List<eDietType> dietOptions)
    {
        _dietDropDown.ClearOptions();
        List<Dropdown.OptionData> newOptions = new List<Dropdown.OptionData>();
        foreach (eDietType diet in System.Enum.GetValues(typeof(eDietType)))
        {
            Dropdown.OptionData d = null;
            if (dietOptions.Contains(diet))
            {
                d = new Dropdown.OptionData(diet.ToString());
            }
            else
            {
                d = new Dropdown.OptionData("---");
            }
            newOptions.Add(d);
        }
        _dietDropDown.AddOptions(newOptions);
    }
    void UpdateUI_Size(eSize size)
    {
        _sizeDropDown.value = ((int)size - 1);
    }
    void UpdateUI_SizeRange(List<eSize> sizeOptions)
    {
        _sizeDropDown.ClearOptions();
        List<Dropdown.OptionData> newOptions = new List<Dropdown.OptionData>();
        foreach (eSize size in System.Enum.GetValues(typeof(eSize)))
        {
            Dropdown.OptionData d = null;
            if (sizeOptions.Contains(size))
            {
                d = new Dropdown.OptionData(size.ToString());
            }
            else
            {
                d = new Dropdown.OptionData("---");
            }
            newOptions.Add(d);
        }
        _sizeDropDown.AddOptions(newOptions);
    }
    void UpdateUI_Name(string name)
    {
        _nameText.text = name;
    }
    void UpdateUI_Population(int pop)
    {
        _populationText.text = "Population: " + pop;
    }

    void UpdateUI_Predators(List<Creature> predators)
    {
        //_predatorsListText.text = "";
        //foreach (Creature crea in predators)
        //{
        //    _predatorsListText.text += crea.Name + ", ";
        //}
        //if(predators.Count > 0)
        //{
        //    //remove trailing comma
        //    _predatorsListText.text = _predatorsListText.text.Substring(0, _predatorsListText.text.Length - 2);
        //}
    }

    void UpdateUI_Prey(List<Creature> prey)
    {
        //_preyListText.text = "";
        //foreach (Creature crea in prey)
        //{
        //    _preyListText.text += crea.Name + ", ";
        //}
        //if (prey.Count > 0)
        //{
        //    //remove trailing comma
        //    _preyListText.text = _preyListText.text.Substring(0, _preyListText.text.Length - 2);
        //}
    }

    void UpdateUI_Extinct(bool extinct)
    {
        _extinctIndicator.SetActive(extinct);
    }

    void UpdateUI_EvolutionCost(float cost)
    {
        _evolutionCostSection.SetActive(cost != 0.0f);
        _evolutionCostValue.text = cost*100 + "%";
    }

    void UpdateUI_MyTurn(bool myTurn)
    {
        _nextTurnButton.SetActive(myTurn);
    }

    //------------------------------------------------------------------------------------------------

    //im torn between transporting properties this way and storing all properties in a containing struct....
    public Creature GetShallowCopy()
    {
        return this.MemberwiseClone() as Creature;
    }

    void OnDestroy()
    {
        //_dietDropDown.onValueChanged.RemoveAllListeners();
        //_sizeDropDown.onValueChanged.RemoveAllListeners();
    }
}
