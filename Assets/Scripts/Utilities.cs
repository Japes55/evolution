﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utilities {

    static string[] nm1 = { "a", "e", "o", "i", "u", "a", "e", "o", "i", "u", "ai", "ea", "eo", "oi", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };
    static string[] nm2 = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "ch", "chr", "chl", "z", "br", "cr", "dr", "fr", "gr", "kr", "pr", "sr", "tr", "str", "bl", "cl", "fl", "kl", "pl", "sl", "vl", "ph", "sh" };
    static string[] nm3 = { "a", "e", "o", "i", "u", "a", "e", "o", "i", "u", "ai", "ea", "eo", "oi", "y" };
    static string[] nm4 = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "r", "s", "t", "v", "w", "x", "z", "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "r", "s", "t", "v", "w", "x", "z", "bl", "br", "bb", "bs", "bd", "bn", "ch", "chl", "chr", "cl", "ck", "cn", "cr", "cc", "dr", "dl", "ds", "dn", "dd", "fl", "ff", "fr", "fn", "gr", "gn", "gs", "gl", "gg", "kl", "kh", "kn", "kk", "kr", "ll", "ln", "lm", "ls", "ld", "lb", "mm", "mn", "md", "ml", "ms", "nn", "nd", "ng", "nt", "ns", "nst", "pp", "ph", "pl", "ps", "pd", "pr", "rr", "rd", "rn", "rl", "rs", "rt", "ss", "sh", "sht", "sl", "sn", "sr", "st", "str", "tr", "tt", "th", "tn", "tm", "tv", "vv", "vl", "vn" };
    static string[] nm7 = { "", "", "", "", "", "", "c", "d", "f", "g", "h", "k", "l", "m", "n", "p", "r", "s", "t", "x", "c", "d", "f", "g", "h", "k", "l", "m", "n", "p", "r", "s", "t", "x", "ch", "ck", "th", "gs", "rd", "rg", "rk", "rm", "rn", "rq", "rs", "rst", "rx", "ds", "cs", "fs", "gs", "ks", "ls", "ms", "ns", "ps", "rs", "ts", "st", "ph", "sh", "ln", "lm", "lk", "ld", "lt" };
    static string[] nm8 = { "c", "gian", "lese", "lian", "n", "nan", "ne", "nee", "nes", "nian", "nin", "no", "nsian", "r", "rd", "rn", "se", "sh", "t", "te", "vese", "vian", "saur" };
    static string[] check = { "anal", "anus", "arse", "ass", "balls", "bastard", "biatch", "bigot", "bitch", "bollock", "bollok", "boner", "boob", "bugger", "bum", "butt", "clitoris", "cock", "coon", "crap", "cunt", "damn", "dick", "dildo", "dyke", "fag", "feck", "felching", "fellate", "fellatio", "flange", "fuck", "gay", "lust", "goddamn", "homo", "jackass", "jerk", "jizz", "knobend", "labia", "muff", "nigga", "nigger", "penis", "piss", "poop", "prick", "pube", "pussy", "queer", "scrotum", "sex", "shit", "slut", "smegma", "spunk", "tit", "tosser", "turd", "twat", "vagina", "wank", "whore", "wtf" };

    public static T GetRandomEnumIndex<T>()
    {
        var values = System.Enum.GetValues(typeof(T));
        int randomIndex = Random.Range(0, values.Length);
        T randomValue = (T)values.GetValue(randomIndex);
        return randomValue;
    }

    //I stole this algorithm from http://www.fantasynamegenerators.com/species-names.php#.WXzE84iGPIU.  
    //I should ask their perimission/forgiveness sometime...
    public static string GenerateRandomName()
    {
        string name = "";
        for (int i = 0; i < 10; i++)
        {
            int rnd = Random.Range(0, nm1.Length);
            int rnd2 = Random.Range(0, nm2.Length);
            int rnd3 = Random.Range(0, nm3.Length);
            if (i < 5)
            {
                int rnd4 = Random.Range(0, nm7.Length);
                name = nm1[rnd] + nm2[rnd2] + nm3[rnd3] + nm7[rnd4];
                for (int j = 0; j < check.Length; j++)
                {
                    while (name == check[j])
                    {
                        rnd = Random.Range(0, nm1.Length);
                        rnd2 = Random.Range(0, nm2.Length);
                        rnd3 = Random.Range(0, nm3.Length);
                        rnd4 = Random.Range(0, nm7.Length);
                        name = nm1[rnd] + nm2[rnd2] + nm3[rnd3] + nm7[rnd4];
                    }
                }
            }
            else
            {
                int rnd4 = Random.Range(0, nm4.Length);
                int rnd6 = Random.Range(0, nm3.Length);
                int rnd7 = Random.Range(0, nm8.Length);
                name = nm1[rnd] + nm2[rnd2] + nm3[rnd3] + nm4[rnd4] + nm3[rnd6] + nm8[rnd7];
                for (int j = 0; j < check.Length; j++)
                {
                    while (name == check[j])
                    {
                        rnd = Random.Range(0, nm1.Length);
                        rnd2 = Random.Range(0, nm2.Length);
                        rnd3 = Random.Range(0, nm3.Length);
                        rnd4 = Random.Range(0, nm4.Length);
                        rnd6 = Random.Range(0, nm3.Length);
                        rnd7 = Random.Range(0, nm8.Length);
                        name = nm1[rnd] + nm2[rnd2] + nm3[rnd3] + nm4[rnd4] + nm3[rnd6] + nm8[rnd7];
                    }
                }
            }
        }

        return name[0].ToString().ToUpper() + name.Substring(1);
    }
}
