﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EvolutionGameController : MonoBehaviour {

    public GameObject CreatureTemplate;

    //UI elements
    Canvas _mainCanvas;
    public GameObject PlantBiomassIndicator;
    Text _plantBiomassText;
    int _plantBioMass;
    public GameObject TurnsSurvivedIndicator;
    Text _turnsSurvivedText;
    int _turnsSurvived;
    public GameObject CreaturesGroup;
    public GameObject GameOverScreen;
    Text _gameOverText;
    public GameObject UpdateEnvironmentButton;

    List<Creature> Creatures;

    const int MAX_PLANT_BIOMASS_LEVEL = 5;
    bool MyTurn
    {
        get
        {
            return _myTurn;
        }
        set
        {
            _myTurn = value;
            UpdateUI_Turn(_myTurn);
        }
    }
    bool _myTurn;

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name); // loads current scene
    }

    // Use this for initialization
    void Start() {

        //get Handles to UI elements
        _mainCanvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        _plantBiomassText = PlantBiomassIndicator.GetComponent<Text>();
        _turnsSurvivedText = TurnsSurvivedIndicator.GetComponent<Text>();
        GameOverScreen.GetComponentInChildren<Button>().onClick.AddListener(Restart);
        _gameOverText = GameOverScreen.transform.Find("Panel/Body").gameObject.GetComponent<Text>();

        int numExtinctCreatures = 0;
        do
        {
            if (Creatures != null)
            {
                foreach (Creature cre in Creatures)
                {
                    Destroy(cre.gameObject);
                }
            }
            Creatures = new List<Creature>();

            //create some creatures...
            float xpos = 200.0f * _mainCanvas.scaleFactor;
            for (int i = 0; i < 4; i++)
            {
                GameObject crea = Instantiate(CreatureTemplate,
                                                new Vector3(xpos, 375.0f * _mainCanvas.scaleFactor, 20.0f),
                                                Quaternion.identity,
                                                CreaturesGroup.transform) as GameObject;
                crea.transform.localScale = Vector3.one;
                Creature creaScript = crea.GetComponent<Creature>();
                creaScript.Initialise(i == 0);
                Creatures.Add(creaScript);
                xpos += 300.0f * _mainCanvas.scaleFactor;
            }

            //start the first round!
            SetPlantBioMassLevel(Random.Range(1, MAX_PLANT_BIOMASS_LEVEL + 1));
            SetInitialPopulations();
            UpdateEnvironment();
            
            numExtinctCreatures = 0;
            foreach(Creature cre in Creatures)
            {
                if(cre.IsExtinct)
                {
                    numExtinctCreatures++;
                }
            }
        }
        while (numExtinctCreatures > 0);

        GameOverScreen.SetActive(false);
        SetTurnsSurvived(0);
    }

    // Update is called once per frame
    void Update() {

    }

    public void HighlightPredatorsPrey(Creature creatureOfInterest)
    {
        foreach (Creature crea in Creatures)
        {
            if (creatureOfInterest != null)
            {
                if (creatureOfInterest.Predators.Contains(crea))
                {
                    crea.Highlight(Creature.ePredatorPreyRelationship.predator);
                }
                else if (creatureOfInterest.Prey.Contains(crea))
                {
                    crea.Highlight(Creature.ePredatorPreyRelationship.prey);
                }
                else
                {
                    crea.Highlight(Creature.ePredatorPreyRelationship.nothing);
                }
            }
            else
            {
                crea.Highlight(Creature.ePredatorPreyRelationship.nothing);
            }
        }
    }

    //sets populations of all creatures to their maximum carrying capacity
    void SetInitialPopulations()
    {
        SortCreatures();
        SetCreaturesMaxBioMasses();
        foreach (Creature crea in Creatures)
        {
            crea.BioMass = crea.MaxBioMass;
            //reset text
            crea.PopulationReasons = "";
            crea.ResetEvolutionCost();
        }
    }

    //creatures have to be sorted for this to work.
    void SetCreaturesMaxBioMasses()
    {
        float plantBioMassPerPlantEater = GetPlantBioMassPerPlantEater();
        foreach (Creature crea in Creatures)
        {
            float bioMassAvailableFromPlants = 0.0f;
            float bioMassAvailableFromPrey = 0.0f;
            if (crea.Diet == Creature.eDietType.herbivore)
            {
                bioMassAvailableFromPlants = plantBioMassPerPlantEater;
                crea.Prey = new List<Creature>(); //set to new instead of clear so it triggers ui update...
            }
            else if (crea.Diet == Creature.eDietType.omnivore)
            {
                //half and half
                bioMassAvailableFromPlants = plantBioMassPerPlantEater / 2;
                bioMassAvailableFromPrey = GetMaxPreyBiomass(crea);
            }
            else if (crea.Diet == Creature.eDietType.carnivore)
            {
                bioMassAvailableFromPrey = GetMaxPreyBiomass(crea);
            }
            crea.MaxBioMass = bioMassAvailableFromPlants + bioMassAvailableFromPrey;

            //Update story
            crea.PopulationReasons += "Max population in this environment: " + 
                                            (int)(crea.MaxBioMass / (float)crea.Size) + " (";
            if(bioMassAvailableFromPlants > 0)
            {
                crea.PopulationReasons += (int)(bioMassAvailableFromPlants / (float)crea.Size) + " from plants";
                if(bioMassAvailableFromPrey > 0)
                {
                    crea.PopulationReasons += ", ";
                }
            }
            if (bioMassAvailableFromPrey > 0)
            {
                crea.PopulationReasons += (int)(bioMassAvailableFromPrey / (float)crea.Size) + " from meat";
            }
            crea.PopulationReasons += ")\n";
        }
    }

    //as a side effect, this sets the prey list of the creature
    float GetMaxPreyBiomass(Creature predator)
    {
        float maxPreyBioMass = 0.0f;
        if (predator.Diet == Creature.eDietType.herbivore)
        {
            return maxPreyBioMass; //veggies don't eat prey, silly
        }

        List<Creature> prey = new List<Creature>();
        foreach (Creature crea in Creatures)
        {
            float predFact = GetPredationFactor(predator, crea);
            if (predFact > 0.0f)
            {
                prey.Add(crea);
                maxPreyBioMass += crea.BioMass * predFact;
            }
        }
        predator.Prey = prey;

        return maxPreyBioMass;
    }

    //returns a value between 0 and 1 that represents the likelihood of a predator catching and eating a given prey creature.
    float GetPredationFactor(Creature predator, Creature prey)
    {
        //remove nonsense cases
        if (predator.Diet == Creature.eDietType.herbivore || prey == predator)
            return 0.0f;

        //has to be right size
        if(prey.Size > predator.Size || prey.Size < predator.Size - 2)
            return 0.0f;

        if (prey.Diet < predator.Diet || prey.Size < predator.Size)
        {
            if (predator.Diet == Creature.eDietType.omnivore)
            {
                return 0.25f; //half again for omnis
            }
            return 0.5f; //50% for now - we will make this awesome later.
        }
        else
        {
            return 0;
        }
    }

    void SortCreatures()
    {
        //we go from small to big and herbivores first
        Creatures.Sort(delegate (Creature x, Creature y)
        {
            //if equal return 0
            //if x greater, return 1
            //if y greater, return -1
            if (x == null && y == null) return 0;
            else if (x == null) return -1;
            else if (y == null) return 1;
            else
            {
                if (x.Size > y.Size)
                {
                    return 1;
                }
                else if (x.Size < y.Size)
                {
                    return -1;
                }
                else
                {
                    if (x.Diet > y.Diet)
                    {
                        return 1;
                    }
                    else if (x.Diet < y.Diet)
                    {
                        return -1;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        });
    }

    void UpdatePlantBioMass()
    {
        int rand = Random.Range(0, 3);
        int bioMassLevel = _plantBioMass / 100; //lame
        if (rand == 0)
        {
            if (bioMassLevel > 1)
            {
                SetPlantBioMassLevel(bioMassLevel - 1);
            }
        }
        else if (rand == 2)
        {
            if (bioMassLevel < MAX_PLANT_BIOMASS_LEVEL)
            {
                SetPlantBioMassLevel(bioMassLevel + 1);
            }
        }
    }

    void SetPlantBioMassLevel(int level)
    {
        _plantBioMass = level*100;
        UpdateUI_PlantBioMass(_plantBioMass);
    }

    void SetTurnsSurvived(int level)
    {
        _turnsSurvived = level;
        _turnsSurvivedText.text = "Periods Survived: " + _turnsSurvived;
    }

    float GetPlantBioMassPerPlantEater()
    {
        float numPlantEaters = 0;
        foreach (Creature crea in Creatures)
        {
            if(crea.Diet == Creature.eDietType.herbivore)
            {
                numPlantEaters++;
            }
            else if (crea.Diet == Creature.eDietType.omnivore)
            {
                numPlantEaters += 0.5f;
            }
        }

        return _plantBioMass / numPlantEaters;
    }

    //creatures have to be sorted for this to work
    void ApplyPredationPenalties()
    {
        foreach (Creature prey in Creatures)
        {
            List<Creature> predators = new List<Creature>();
            foreach (Creature predator in Creatures)
            {
                float predFact = GetPredationFactor(predator, prey);
                if (predFact > 0.0f)
                {
                    //exchange some energy...
                    prey.PopulationReasons += "-" + (int)((predator.BioMass * predFact) / (float)prey.Size) +
                                                " due to predation by " + predator.Name + "\n";
                    prey.BioMass -= predator.BioMass * predFact;
                    predators.Add(predator);
                }
            }
            prey.Predators = predators;
        }
    }

    void AddPopulationNoise()
    {
        foreach (Creature crea in Creatures)
        {
            int rand = Random.Range(0, 4);
            if(rand == 1)
            {
                crea.PopulationReasons += "+" + (int)(crea.BioMass * 0.1f / (float)crea.Size) + " growth due to good luck\n";
                crea.BioMass *= 1.1f;
            }
            else if (rand == 4)
            {
                crea.PopulationReasons += "-" + (int)(crea.BioMass * 0.1f / (float)crea.Size) + " growth due to bad luck\n";
                crea.BioMass *= 0.9f;
            }
        }
    }

    void GrowPopulations()
    {
        foreach (Creature crea in Creatures)
        {
            //rules for growth - grow till you're halfway between where you are and the max,
            float growth = (crea.MaxBioMass - crea.BioMass) / 2;
            crea.PopulationReasons += "+" + (int)(growth / (float)crea.Size) + " growth due to available food\n";
            crea.BioMass += growth;
        }
    }

    void RemoveExtinctCreaturesFromList()
    {
        //remove extinct dudes from our calculations for this turn:
        List<Creature> creaturesToRemove = new List<Creature>();
        foreach (Creature c in Creatures)
        {
            if (c.IsExtinct)
            {
                creaturesToRemove.Add(c);
            }
        }
        foreach (Creature c in creaturesToRemove)
        {
            Creatures.Remove(c);
        }
    }

    public void OnNextTurnClick()
    {
        UpdateMyCreature();
        MyTurn = false;
    }

    void UpdateMyCreature()
    {
        RemoveExtinctCreaturesFromList();
        foreach (Creature c in Creatures)
        {
            //reset strings
            c.PopulationReasons = "Population last period: " + c.Population + "\n";
            if (c.IsPlayer)
            {
                c.Evolve();
            }
        }
        EvaluatePopulations();
        MyTurn = false;
    }

    //iterates a next turn
    public void UpdateEnvironment()
    {
        RemoveExtinctCreaturesFromList();
        UpdatePlantBioMass();
        //randomly evolve the non-player creatures, handle evolution for player character
        foreach (Creature c in Creatures)
        {
            //reset strings
            c.PopulationReasons = "Population last period: " + c.Population + "\n";
            if (!c.IsPlayer)
            {
                //randomly tweak
                c.Mutate();
            }
        }
        EvaluatePopulations();
        MyTurn = true;
    }

    void EvaluatePopulations()
    {
        SortCreatures();
        SetCreaturesMaxBioMasses();

        ApplyPredationPenalties();

        GrowPopulations();

        AddPopulationNoise();

        //right at the end, check if they're extict.
        foreach (Creature c in Creatures)
        {
            if (c.Population <= 0)
            {
                c.SetExtinct();
                if (c.IsPlayer)
                {
                    //GAME OVER - show gameover screen
                    GameOverScreen.SetActive(true);
                    _gameOverText.text = "Your line has ended, your species is no more.\nYou survived: " + _turnsSurvived +
                        " periods.";
                    return;
                }
            }
        }

        SetTurnsSurvived(_turnsSurvived + 1);
    }

    //UI update methods------------------------------------------------------------------------------------------
    void UpdateUI_Turn(bool myTurn)
    {
        foreach (Creature c in Creatures)
        {
            if (c.IsPlayer)
            {
                c.SetMyTurn(myTurn);
            }
        }

        UpdateEnvironmentButton.SetActive(!myTurn);

    }

    void UpdateUI_PlantBioMass(int plantBioMass)
    {
        _plantBiomassText.text = "Plant energy: ";
        int counter = plantBioMass;
        while(counter > 0)
        {
            _plantBiomassText.text += "====";
            counter -= 50;
        }
    }

}
