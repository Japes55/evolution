﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseOverToolTip : MonoBehaviour {

    RectTransform _rect;
    Canvas _mainCanvas;
    Text _text;

    // Use this for initialization
    void Start () {
    }

    public void Init()
    {
        _rect = GetComponent<RectTransform>();
        _mainCanvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        _text = GetComponentInChildren<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        float widthOffset = (_rect.rect.width / 2) * _mainCanvas.scaleFactor;
        float heightOffset = (-_rect.rect.height / 2) * _mainCanvas.scaleFactor;
        transform.position = Input.mousePosition + new Vector3(widthOffset, heightOffset, 0);
	}

    //todo make this look awesome, seperate text elements coloured red or green if they're positive or negative
    public void SetText(string msg)
    {
        _text.text = msg;
    }
}
